This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm install
npm run dev
# or
yarn
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Plus d'info

- ~9h de travail
- On observe parfois deux cadres sur certains objets. Il faudrait normaliser les temps, càd tout "arrondir" à des multiples de la durée d'une frame pour éviter que 2 cadres puissent entrer dans la même frame.
