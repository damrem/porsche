import type { NextPage } from "next";
import { useCallback, useEffect, useRef, useState } from "react";
import { API_BASE_URL, VIDEO_HEIGHT, VIDEO_WIDTH } from "../config";
import styles from "../styles/Home.module.css";
import videoStyles from "../styles/video.module.css";
import { BoxInterpolator } from "../components/BoxInterpolator";
import { parseAnalysis } from "../parsing/parseAnalysis";
import { Interpolation } from "../parsing/Interpolation";

export async function getServerSideProps(): Promise<{
  props: { interpolations: Interpolation[] };
}> {
  const res = await fetch(`${API_BASE_URL}/detections`);
  const { data } = await res.json();
  return { props: { interpolations: parseAnalysis(data.analysis) } };
}

const Home: NextPage<{ interpolations: Interpolation[] }> = ({
  interpolations,
}) => {
  const videoRef = useRef<HTMLVideoElement>(null);
  const [currentTime, setCurrentTime] = useState(0);

  const onTimeUpdate = useCallback(() => {
    if (!videoRef.current?.currentTime) return;

    setCurrentTime(videoRef.current?.currentTime * 1000);
  }, []);

  useEffect(() => {
    if (videoRef.current === null) return;

    videoRef.current.ontimeupdate = onTimeUpdate;
  }, [onTimeUpdate, videoRef]);

  return (
    <div className={styles.container}>
      <div className={videoStyles.container}>
        <video
          controls
          ref={videoRef}
          className={videoStyles.video}
          width={VIDEO_WIDTH}
          height={VIDEO_HEIGHT}
        >
          <source src="porshe.mp4" type="video/mp4" />
          Your browser does not support the video tag.
        </video>

        <BoxInterpolator
          svgClassName={videoStyles.boxes}
          interpolations={interpolations}
          currentTime={currentTime}
        />
      </div>
    </div>
  );
};

export default Home;
