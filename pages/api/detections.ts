import { Analysis } from "../../models/Analysis";
import { NextApiRequest, NextApiResponse } from "next";

import detections from "../../resources/porshe-video-object-detections.json";

export default function handler(
  _: NextApiRequest,
  res: NextApiResponse<{ data: { analysis: Analysis } }>
) {
  res.status(200).json(detections as { data: { analysis: Analysis } });
}
