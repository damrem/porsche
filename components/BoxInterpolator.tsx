import { easeLinear } from "d3";
import { useMemo } from "react";
import {
  VIDEO_HALF_PERIOD,
  VIDEO_HEIGHT,
  VIDEO_PERIOD,
  VIDEO_WIDTH,
} from "../config";
import { useD3 } from "../d3/useD3";
import { Interpolation } from "../parsing/Interpolation";

const byId = (d: unknown) => (d as Interpolation).id;
const duration = (d: Interpolation) => d.duration ?? 0;
const label = (d: Interpolation) => d.label;

const fromWidth = (d: Interpolation) => d.from.width;
const fromHeight = (d: Interpolation) => d.from.height;
const fromTransform = (d: Interpolation) =>
  `translate (${d.from.x}, ${d.from.y})`;

const toX = (d: Interpolation) => d.to?.x ?? d.from.x;
const toY = (d: Interpolation) => d.to?.y ?? d.from.y;
const toWidth = (d: Interpolation) => d.to?.width ?? d.from.width;
const toHeight = (d: Interpolation) => d.to?.height ?? d.from.height;
const toTransform = (d: Interpolation) => `translate (${toX(d)}, ${toY(d)})`;

export function BoxInterpolator({
  currentTime,
  interpolations,
  svgClassName,
}: {
  currentTime: number;
  interpolations: Interpolation[];
  svgClassName?: string;
}) {
  //   this memoization would probably be more efficient if currentTime was "rounded" (only multiples of frame period: ~0.1667)
  const currentInterpolations = useMemo(
    () =>
      interpolations.filter((d) => {
        const delta = d.time - currentTime;
        return delta < VIDEO_PERIOD && delta > 0;
      }),
    [currentTime, interpolations]
  );

  const ref = useD3(
    (svg) => {
      const selection = svg.selectAll("g").data(currentInterpolations, byId);
      selection.exit().remove();

      const rects = svg.selectAll("rect").data(currentInterpolations, byId);

      const entered = selection.enter();

      const graphics = entered
        .append("g")
        .attr("id", byId)
        .attr("transform", fromTransform);

      graphics

        .append("rect")
        .attr("stroke", "white")
        .attr("fill", "none")
        .attr("width", fromWidth)
        .attr("height", fromHeight);

      graphics.append("text").text(label).attr("fill", "white");

      selection
        .transition()
        .duration(duration)
        .ease(easeLinear)
        .attr("transform", toTransform);

      rects
        .transition()
        .ease(easeLinear)
        .duration(duration)
        .attr("width", toWidth)
        .attr("height", toHeight);
    },
    [interpolations?.length]
  );

  return (
    <svg
      className={svgClassName}
      ref={ref}
      width={VIDEO_WIDTH}
      height={VIDEO_HEIGHT}
    ></svg>
  );
}
