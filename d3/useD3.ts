import { useRef } from "react";

import { select, Selection } from "d3";
import { useDeepCompareEffect } from "react-use";

type RenderChartFn = (
  selection: Selection<SVGSVGElement, unknown, null, undefined>
) => void;

//  adapted from https://www.pluralsight.com/guides/using-d3.js-inside-a-react-app
export function useD3(renderChartFn: RenderChartFn, dependencies: unknown[]) {
  const ref = useRef<SVGSVGElement>(null);

  useDeepCompareEffect(() => {
    if (!ref.current) return;

    const chart = select(ref.current);
    if (!chart) return;

    renderChartFn(chart);
  }, [dependencies, renderChartFn, ref]);

  return ref;
}
