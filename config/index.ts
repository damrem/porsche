export const BASE_URL = "http://localhost:3000";
export const API_BASE_URL = `${BASE_URL}/api`;

export const VIDEO_WIDTH = 1280;
export const VIDEO_HEIGHT = 720;
export const VIDEO_PERIOD = 10000 / 60; //  écart de temps observé dans l'analyse de la vidéo
export const VIDEO_HALF_PERIOD = VIDEO_PERIOD / 2;
