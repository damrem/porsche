import { ObjectClass } from "../models/Analysis";

type Rect = {
  x: number;
  y: number;
  width: number;
  height: number;
};

export type ObjectClassLabel = "Person" | "Car";

export type Interpolation = {
  id: string;
  label: ObjectClassLabel;
  time: number;
  from: Rect;
  to: Rect | null;
  duration: number | null;
};
