import { Interpolation, ObjectClassLabel } from "./Interpolation";
import {
  Analysis,
  AnalysisObject,
  Appearance,
  ObjectClass,
  SpaceBox,
  TimeBox,
} from "../models/Analysis";

const parseSpaceBox = (box: SpaceBox) => {
  const width = box.bottomRight.x - box.topLeft.x;
  const height = box.bottomRight.y - box.topLeft.y;
  return { ...box.topLeft, width, height };
};

const parseObjectClass: (objectClass: ObjectClass) => ObjectClassLabel = (
  objectClass
) => {
  switch (objectClass) {
    case "OBJECT_CLASS_CAR":
      return "Car";
    case "OBJECT_CLASS_PERSON":
      return "Person";
  }
};

function parseTimeBox(
  timeBox: TimeBox,
  id: string,
  objectClass: ObjectClass,
  nextTimeBox: TimeBox | null,
  appearanceIndex: number
): Interpolation {
  const to = nextTimeBox ? parseSpaceBox(nextTimeBox.box) : null;
  const duration = nextTimeBox ? nextTimeBox.time - timeBox.time : null;
  const idParts = id.split("/");
  const objectId = `${idParts[idParts.length - 1]}-${appearanceIndex}`;
  return {
    from: parseSpaceBox(timeBox.box),
    time: timeBox.time,
    id: objectId,
    label: parseObjectClass(objectClass),
    to,
    duration,
  };
}

function parseAppearance(
  appearance: Appearance,
  id: string,
  objectClass: ObjectClass,
  appearanceIndex: number
): Interpolation[] {
  return appearance.boxes.map((box, i, boxes) =>
    parseTimeBox(
      box,
      id,
      objectClass,
      i < boxes.length - 1 ? boxes[i + 1] : null,
      appearanceIndex
    )
  );
}

const flatten = <T>(prev: T[], curr: T[]) => [...prev, ...curr];

function parseAnalysisObject(analysisObject: AnalysisObject): Interpolation[] {
  return analysisObject.appearances
    .map((appearance, i) =>
      parseAppearance(
        appearance,
        analysisObject.id,
        analysisObject.objectClass,
        i
      )
    )
    .reduce(flatten, []);
}

export function parseAnalysis(analysis: Analysis): Interpolation[] {
  return analysis.objects.map(parseAnalysisObject).reduce(flatten, []);
}
