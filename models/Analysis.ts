export type Coord = { x: number; y: number };

export type SpaceBox = { bottomRight: Coord; topLeft: Coord };

export type TimeBox = { box: SpaceBox; time: number };

export type Appearance = { boxes: TimeBox[] };

export type ObjectClass = "OBJECT_CLASS_CAR" | "OBJECT_CLASS_PERSON";

export type AnalysisObject = {
  appearances: Appearance[];
  id: string;
  objectClass: ObjectClass;
};

export type Analysis = { objects: AnalysisObject[] };
